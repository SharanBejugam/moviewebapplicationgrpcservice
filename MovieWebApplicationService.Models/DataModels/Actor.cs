﻿using System.ComponentModel.DataAnnotations;

namespace MovieWebApplicationService.Models.DataModels
{
    public class Actor
    {
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        [Required]
        [Range(15, 80)]
        public int Age { get; set; }
        public string? Gender { get; set; }
    }
}
