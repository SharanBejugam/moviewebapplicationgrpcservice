﻿namespace MovieWebApplicationService.Models.DataModels
{
    public class Producer
    {
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public int Age { get; set; }
        public string? Gender { get; set; }
    }
}
