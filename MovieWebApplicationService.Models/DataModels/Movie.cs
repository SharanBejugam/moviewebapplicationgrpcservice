﻿namespace MovieWebApplicationService.Models.DataModels
{
    public class Movie
    {
        private Guid Id;
        public string GetId()
        {
            return Id.ToString();
        }
        public void SetGuid(string value)
        {
            Id = Guid.Parse(value);
        }
        public string? MovieName { get; set; }
        public string? ReleaseDate { get; set; }
        public string? Actors { get; set; }
        public string? Directors { get; set; }
        public string? Producers { get; set; }
        public int Rating { get; set; }
        public long TotalCost { get; set; }
        public string? Description { get; set; }

    }
}
