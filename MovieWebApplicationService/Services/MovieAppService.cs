﻿using AutoMapper;
using Grpc.Core;
using MediatR;
using MovieWebApplicationService.CQRS.Commands;
using MovieWebApplicationService.CQRS.Queries;
using MovieWebApplicationService.Protos;

namespace MovieWebApplicationService.Services
{
    public class MovieAppService : MovieService.MovieServiceBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public MovieAppService(IMediator sender, IMapper mapper) 
        { 
            _sender = sender; 
            _mapper = mapper;
        }

        public override async Task<MovieList> GetAllMovies(Empty request, ServerCallContext context)
        {
            var movies = await _sender.Send(new GetAllMoviesQuery());
            MovieList response = new MovieList();
            foreach(var movie in movies)
            {
                response.Items.Add(_mapper.Map<Movie>(movie));
            }
            return response;
        }

        public override async Task<Movie> GetMovieById(MovieId request, ServerCallContext context)
        {
            var movie = await _sender.Send(new GetMovieByIdQuery(Guid.Parse(request.Id)));
            Movie response = _mapper.Map<Movie>(movie);
            return response;
        }

        public override async Task<OperationStatus> AddMovie(Movie request, ServerCallContext context)
        {
            var newMovie = new Movie
            {
                Id = request.Id,
                MovieName = request.MovieName,
                ReleaseDate = request.ReleaseDate,
                Actors = request.Actors,
                Directors = request.Directors,
                Producers = request.Producers,
                Rating = request.Rating,
                TotalCost = request.TotalCost,
                Description = request.Description
            };
            var operationstatus = await _sender.Send(new AddMovieCommand(newMovie));
            OperationStatus response = operationstatus;
            return response;
        }
    }
}