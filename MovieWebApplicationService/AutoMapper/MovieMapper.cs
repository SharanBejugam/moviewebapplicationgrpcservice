﻿using AutoMapper;
using MovieWebApplicationService.Protos;

namespace MovieWebApplicationService.AutoMapper
{
    public class MovieMapper : Profile
    {
        public MovieMapper() 
        {
            CreateMap<Models.DataModels.Movie, MovieList>().ReverseMap();
        }
    }
}
