﻿using MediatR;
using MovieWebApplicationService.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.CQRS.Queries
{
    public record GetAllMoviesQuery : IRequest<List<Movie>>;
}
