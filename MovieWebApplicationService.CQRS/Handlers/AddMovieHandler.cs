﻿using MediatR;
using MovieWebApplicationService.CQRS.Commands;
using MovieWebApplicationService.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.CQRS.Handlers
{
    public class AddMovieHandler : IRequestHandler<AddMovieCommand, bool>
    {
        private readonly IMovieRepository repo;

        public AddMovieHandler(IMovieRepository _repo) => repo = _repo;

        public async Task<bool> Handle(AddMovieCommand request, CancellationToken cancellationToken)
        {
            return await repo.AddMovie(request.movie);
        }
    }
}
