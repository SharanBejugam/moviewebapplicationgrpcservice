﻿using MediatR;
using MovieWebApplicationService.CQRS.Queries;
using MovieWebApplicationService.DataAccess.Interfaces;
using MovieWebApplicationService.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.CQRS.Handlers
{
    public class GetAllMoviesHandler : IRequestHandler<GetAllMoviesQuery, List<Movie>>
    {
        private readonly IMovieRepository _repo;

        public GetAllMoviesHandler(IMovieRepository repo) => _repo = repo;

        public async Task<List<Movie>> Handle(GetAllMoviesQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetAllMovies();
        }
    }
}
