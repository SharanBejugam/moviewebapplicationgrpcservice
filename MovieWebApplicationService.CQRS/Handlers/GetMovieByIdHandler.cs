﻿using MediatR;
using MovieWebApplicationService.CQRS.Queries;
using MovieWebApplicationService.DataAccess.Interfaces;
using MovieWebApplicationService.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.CQRS.Handlers
{
    public class GetMovieByIdHandler : IRequestHandler<GetMovieByIdQuery, Movie>
    {
        private readonly IMovieRepository repo;

        public GetMovieByIdHandler(IMovieRepository _repo) => repo = _repo;

        public async Task<Movie> Handle(GetMovieByIdQuery request, CancellationToken cancellationToken)
        {
            return await repo.GetMovieById(request.Id);
        }
    }
}
