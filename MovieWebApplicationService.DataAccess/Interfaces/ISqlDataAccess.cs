﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.DataAccess.Interfaces
{
    public interface ISqlDataAccess
    {
        Task<IEnumerable<T>> LoadData<T>(string sqlQuery, object parameters);
        Task SaveData(string sqlquery, object parameters);
    }
}
