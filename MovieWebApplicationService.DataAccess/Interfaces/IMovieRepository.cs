﻿using MovieWebApplicationService.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieWebApplicationService.DataAccess.Interfaces
{
    public interface IMovieRepository
    {
        Task<bool> AddMovie(Movie movie);
        Task<List<Movie>> GetAllMovies();
        Task<Movie> GetMovieById(Guid id);
        Task<bool> DeleteMovie(Guid id);
        Task<bool> UpdateMovie(Movie movie);
    }
}
